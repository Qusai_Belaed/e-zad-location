import 'package:location/data/model/product.dart';

import 'media.dart';

class Brand {
  String id;
  String name;
  String categoryId;
  Media image;
  List<Product> products;

  Brand();

  Brand.fromJson(Map<String, dynamic> json) {
    try {
      id = json['id'].toString();
      name = json['name'];
      categoryId = json['category_id'].toString();
      image = json['media'] != null && (json['media'] as List).length > 0
          ? Media.fromJson(json['media'][0])
          : new Media();
      if (json['products'] != null) {
        products = new List<Product>();
        json['products'].forEach((v) {
          products.add(new Product.fromJson(v));
        });
      }
    } catch (e) {
      id = '';
      name ='';
      categoryId ='';
      products = [];
      print(e);
    }
    }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category_id'] = this.categoryId;
    data['image'] = this.image;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}