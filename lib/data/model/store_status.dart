class StoreStatus {
  int id;
  String name;

  StoreStatus(this.id, this.name);

  static List<StoreStatus> getCompanies() {
    return <StoreStatus>[
      StoreStatus(0, 'مفتوح'),
      StoreStatus(1, 'مغلق'),
    ];
  }
}