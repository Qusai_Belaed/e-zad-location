import '../api_url.dart';

class Media {
  String id;
  String name;
  String url;

  Media(){
    url=base_url +'img/image_default.png';
  }

  Media.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    name = json['name'];
    url = json['url'];
    print('===============================');
    print(url);
    print('===============================');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['url'] = this.url;
    return data;
  }
}