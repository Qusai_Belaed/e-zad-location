
import 'brand.dart';

class Category {
  String id;
  String name;
  List<Brand> brands;

  Category();

  Category.fromJson(Map<String, dynamic> json) {
    try {
      id = json['id'].toString();
      name = json['name'];
      if (json['brands'] != null) {
        brands = new List<Brand>();
        json['brands'].forEach((v) {
          brands.add(new Brand.fromJson(v));
        });
      }
    } catch (e) {
      id = '';
      name ='';
      brands = [];
      print(e);
    }
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.brands != null) {
      data['brands'] = this.brands.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


