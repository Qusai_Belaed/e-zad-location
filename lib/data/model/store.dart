import 'package:equatable/equatable.dart';
import 'package:http/http.dart';

class Store extends Equatable{
  int id;
  String name;
  String outletCode;
  String owner;
  String seller;
  String address;
  double lat;
  double lng;
  String plusCode;
  String phone;
  String phone2;
  String email;
  int sizeId;
  int counterNo;
  int storeStute;
  int isStore;
  int cityId;
  int zoneId;
  int activityTypeId;
  int userId;
  List<MultipartFile> pictures;
  List<dynamic> payment;
  String createdAt;
  String updatedAt;
  Null deletedAt;

  Store(
      {
        this.id,
        this.name,
        this.outletCode,
        this.owner,
        this.seller,
        this.address,
        this.lat,
        this.lng,
        this.plusCode,
        this.phone,
        this.phone2,
        this.email,
        this.sizeId,
        this.counterNo,
        this.storeStute,
        this.isStore,
        this.cityId,
        this.zoneId,
        this.activityTypeId,
        this.userId,
        this.pictures,
        this.payment,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Store.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    outletCode = json['outlet_code'];
    owner = json['owner'];
    seller = json['seller'];
    address = json['address'];
    lat = json['lat'];
    lng = json['lng'];
    plusCode = json['plus_code'];
    phone = json['phone'];
    phone2 = json['phone2'];
    email = json['email'];
    sizeId = json['size_id'];
    counterNo = json['counter_no'];
    storeStute = json['store_stute'];
    isStore = json['is_store'];
    cityId = json['city_id'];
    zoneId = json['zone_id'];
    activityTypeId = json['activity_type_id'];
    userId = json['user_id'];
    payment = json['payment[]'];
    pictures = json['Picture_url[]'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['outlet_code'] = this.outletCode;
    data['owner'] = this.owner;
    data['seller'] = this.seller;
    data['address'] = this.address;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['plus_code'] = this.plusCode;
    data['phone'] = this.phone;
    data['phone2'] = this.phone2;
    data['email'] = this.email;
    data['size_id'] = this.sizeId;
    data['counter_no'] = this.counterNo;
    data['store_stute'] = this.storeStute;
    data['is_store'] = this.isStore;
    data['city_id'] = this.cityId;
    data['zone_id'] = this.zoneId;
    data['activity_type_id'] = this.activityTypeId;
    data['user_id'] = this.userId;
    data['Picture_url[]'] = this.pictures;
    data['payment[]'] = this.payment;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }

  @override
  // TODO: implement props
  List<Object> get props => [id , name , outletCode , owner , seller , address , lat , lng , plusCode , phone , phone2 , email , sizeId , counterNo , storeStute , isStore , cityId , zoneId , userId , pictures , payment , createdAt , updatedAt , deletedAt];
}
