import 'package:equatable/equatable.dart';

class ActivityType extends Equatable{
  int _id;
  String _name;

  ActivityType({int id, String name}) {
    this._id = id;
    this._name = name;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;

  ActivityType.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    return data;
  }

  @override
  // TODO: implement props
  List<Object> get props => [_id , _name];
}