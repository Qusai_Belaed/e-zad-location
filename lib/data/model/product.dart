
class Product {
  String id;
  String name;
  String barcode;
  String brandId;


  Product();

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    name = json['name'];
    barcode = json['barcode'].toString();
    brandId = json['brand_id'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['barcode'] = this.barcode;
    data['brand_id'] = this.brandId;
    return data;
  }


}