import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:location/data/model/store.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api_url.dart';
abstract class Repository {
  Future<String> postStore(Store store);
}

class RepositoryImpl implements Repository {
  SharedPreferences prefs;
  http.MultipartRequest request;
  Future<String> postStore(Store store) async {
    try{
//      String storeUrl = api_url + "stores";
//      Map<String, String> headers = {
//        'Accept': 'application/json',
////      'Authorization': 'Bearer $token'
//      };
//      Map<String , dynamic> data = {
//        "name" : store.name,
//        "owner" : store.owner,
//        "seller" : store.seller,
//        "address" : store.address,
//        "lat" : store.address,
//        "lng" : store.lng,
//        "plusCode" : store.plusCode,
//        "phone" : store.phone.toString(),
//        "phone2" : store.phone2.toString(),
//        "email" : store.email,
//        "size_id" : store.sizeId,
//        "counter_no" : store.counterNo,
//        "store_stute" : store.storeStute,
//        "is_store" : store.isStore,
//        "zone_id" : store.zoneId,
//        "city_id" : store.cityId,
//        "activity_type_id" : store.activityTypeId,
//        "payment[0]" : 1
//      };
//      Dio dio = Dio();
//      dio.interceptors.add(PrettyDioLogger());
//      Response response = await dio.post(storeUrl , data: data , options: Options(headers: headers));
      prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token');
      String storeUrl = api_url + "stores";
      Map<String, String> headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      var uri = Uri.parse(storeUrl);
      request = http.MultipartRequest('POST', uri);
      request.headers.addAll(headers);
      request.fields['name'] = store.name;
      request.fields['owner'] = store.owner;
      request.fields['seller'] = store.seller;
      request.fields['address'] = store.address;
      request.fields['lat'] = store.lat.toString();
      request.fields['lng'] = store.lng.toString();
      request.fields['plus_code'] = store.plusCode;
      request.fields['phone'] = store.phone.toString();
      request.fields['phone2'] = store.phone2.toString();
      request.fields['email'] = store.email;
      request.fields['size_id'] = store.sizeId.toString();
      request.fields['counter_no'] = store.counterNo.toString();
      request.fields['store_stute'] = store.storeStute.toString();
      request.fields['is_store'] = store.isStore.toString();
      request.fields['zone_id'] = store.zoneId.toString();
      request.fields['city_id'] = store.cityId.toString();
      request.fields['activity_type_id'] = store.activityTypeId.toString();
      var i = 0;
      store.payment.forEach((element) {
        request.fields['payment[$i]'] = element.toString();
        i++;
      });
      request.files.addAll(store.pictures);
      var response = await request.send();
      print(response.statusCode);
      print(response.contentLength);
      print(response.request);
      if (response.statusCode == 200) {
        return "success";
      }else{
        return "failed";
      }
    }catch(_){
        print(">> Error : " + _);
    }
  }
}
