import 'dart:async';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

class UserRepository {
  static const AUTH_TOKEN = "access_token";
  final String _url = api_url+"login";
  var status;
  var token;
  var data;
  SharedPreferences prefs;
  final _storage = FlutterSecureStorage();
  Future<String> authenticate({
    @required String username,
    @required String password,
  }) async {
    prefs = await SharedPreferences.getInstance();
    try{
      final response = await  http.post(_url,
          headers: {
            'Accept':'application/json'
          },
          body: {
            "email_phone": "$username",
            "password" : "$password"
          });
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        print('data : ${data["access_token"]}');
        prefs.setString('token', data["access_token"]);
        return data["access_token"];
      }else {
        throw Exception('اسم المستخدم أو كلمة المرور غير صحيحه');
      }
    }catch(_){
      throw Exception('اسم المستخدم أو كلمة المرور غير صحيحه');
    }
    }

  Future<void> deleteToken() async {
    await _storage.delete(key: AUTH_TOKEN);
    Logger.root.info("Deleted token.");
  }

  Future<void> persistToken(String token) async {
    await _storage.write(key: AUTH_TOKEN, value: "Bearer $token");
    Logger.root.info("Persisted token.");
  }

  Future<String> hasToken() async {
    final String token = await _storage.read(key: AUTH_TOKEN);

    if (token != null) {
      Logger.root.info("Has token!");
    } else {
      Logger.root.info("Does not have token!");
    }

    return token;
  }
}