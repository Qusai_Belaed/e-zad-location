import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:location/data/model/activity_type.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class ActivityTypeRepository{
  Future<List<ActivityType>> fetch_all_activity_type();
}


class ApiActivityTypeRepository extends ActivityTypeRepository {

  SharedPreferences prefs;

  @override
  Future<List<ActivityType>> fetch_all_activity_type() async{
    // TODO: implement _fetch_all_cities
     prefs = await SharedPreferences.getInstance();
     String token =  prefs.getString('token');
     try{
       String activity_type_url = api_url+"activityTypes";
       var response = await http.get(
           activity_type_url,
           headers: {
             'Accept':'application/json',
             'Authorization' : 'Bearer $token'
           });

       var activities = List<ActivityType>();
       if (response.statusCode == 200) {
         var activitiesJson = json.decode(response.body);
         print(activitiesJson);
         for (var activityJson in activitiesJson['data']) {
           activities.add(ActivityType.fromJson(activityJson));
         }
         print(activities);
         return activities;
       } else {
         throw Exception('Failed to get Activities');
       }
     } catch(_){
       throw Exception(_);
     }
  }

}