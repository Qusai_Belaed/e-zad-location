import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:location/data/model/locations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api_url.dart';

abstract class LocationRepository{
  Future<List<Locations>> fetch_all_locations();
}

class ApiLocationRepository extends LocationRepository {

  SharedPreferences prefs;
  @override
  Future<List<Locations>> fetch_all_locations() async{
    // TODO: implement _fetch_all_cities
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');
    try{
      String locationsUrl = api_url+"get-store";
      var response = await http.get(
      locationsUrl,
      headers: {
        'Accept':'application/json',
        'Authorization' : 'Bearer $token'
      });
      var locations = List<Locations>();
      if (response.statusCode == 200) {
        var locationsJson = json.decode(response.body);
        print(locationsJson);
        for (var locationJson in locationsJson['data']) {
          locations.add(Locations.fromJson(locationJson));
        }
        return locations;
      } else {
        throw Exception('فشل في الاتصال بالانترنت');
      }
    } catch(_){
      throw Exception("فشل في الاتصال بالانترنت");
    }
  }
}