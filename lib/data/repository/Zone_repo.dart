import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:location/data/model/Zone.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class ZoneRepository{
  Future<List<Zone>> fetchAllZones(int id);
  Future<dynamic> addNewZone(String name , int city_id);
}


class ApiZoneRepository extends ZoneRepository {

  SharedPreferences prefs;
  @override
  Future<List<Zone>> fetchAllZones(int id) async{
    // TODO: implement _fetch_all_cities
     prefs = await SharedPreferences.getInstance();
     String token =  prefs.getString('token');
    try{
      String zonesUrl = api_url+"cities/$id/zones";
      var response = await http.get(
          zonesUrl,
          headers: {
            'Accept':'application/json',
            'Authorization' : 'Bearer $token'
          });
      var zones = List<Zone>();
      if (response.statusCode == 200) {
        var zonesJson = json.decode(response.body);
        print(zonesJson);
        for (var zoneJson in zonesJson['data']) {
          zones.add(Zone.fromJson(zoneJson));
        }
        print(zones);
        return zones;
      } else {
        throw Exception('Failed to get Zones');
      }
    } catch(_){
        throw Exception(_);
    }
  }

  @override
  Future<dynamic> addNewZone(String name, int city_id) async{
    // TODO: implement addNewZone
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');
      String zonesUrl = api_url+"zones";
      var respons = await http.post(
          zonesUrl,
          headers: {
            'Accept':'application/json',
            'Authorization' : 'Bearer $token'
          },
          body: {
            "name": '$name',
            "lat": '0.0',
            "lng": '0.0',
            "city_id": city_id.toString()
          }
      );
      print(respons.statusCode);
      print(respons.body);
  }

}
