import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:location/data/model/payment.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api_url.dart';

abstract class PaymentRepository{
  Future<List<Payment>> fetch_all_payments();
}

class ApiPaymentRepository extends PaymentRepository {

  SharedPreferences prefs;
  @override
  Future<List<Payment>> fetch_all_payments() async{
    // TODO: implement _fetch_all_payments
     prefs = await SharedPreferences.getInstance();
     String token =  prefs.getString('token');
     try{
       String paymentsUrl = api_url+"payments";
       var response = await http.get(
           paymentsUrl,
           headers: {
             'Accept':'application/json',
             'Authorization' : 'Bearer $token'
           });
       var payments = List<Payment>();
       if (response.statusCode == 200) {
         var paymentsJson = json.decode(response.body);
         print(paymentsJson);
         for (var paymentJson in paymentsJson['data']) {
           payments.add(Payment.fromJson(paymentJson));
         }
         print(payments);
         return payments;
       } else {
         throw Exception('Failed to get payments');
       }
     } catch(_){
       throw Exception(_);
     }
  }
}