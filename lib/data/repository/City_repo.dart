import 'dart:convert';

//import 'package:city/data/model/City.dart';
import 'package:http/http.dart' as http;
import 'package:location/data/model/city.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class CityRepository{
  Future<List<City>> fetch_all_cities();
}


class ApiCityRepository extends CityRepository {

  SharedPreferences prefs;


  @override
  Future<List<City>> fetch_all_cities() async{
    // TODO: implement _fetch_all_cities
     prefs = await SharedPreferences.getInstance();
     String token =  prefs.getString('token');
     try{
       String citiesUrl = api_url+"cities";
       var response = await http.get(
           citiesUrl,
           headers: {
             'Accept':'application/json',
             'Authorization' : 'Bearer $token'
           });

       var cities = List<City>();
       if (response.statusCode == 200) {
         var citiesJson = json.decode(response.body);
         print(citiesJson);
         for (var cityJson in citiesJson['data']) {
           cities.add(City.fromJson(cityJson));
         }
         print(cities);
         return cities;
       } else {
         throw Exception('فشل في الاتصال بالانترنت');
       }
     } catch(_){
       throw Exception("فشل في الاتصال بالانترنت");
     }
  }
}