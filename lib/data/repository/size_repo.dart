import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:location/data/model/size.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class SizeRepository{
  Future<List<Size>> fetchAllSizes();
}


class ApiSizeRepository extends SizeRepository {

  SharedPreferences prefs;

  @override
  Future<List<Size>> fetchAllSizes() async{
    // TODO: implement _fetch_all_cities
     prefs = await SharedPreferences.getInstance();
     String token =  prefs.getString('token');
    try{
      String sizesUrl = api_url+"sizes";
      var response = await http.get(
          sizesUrl,
          headers: {
            'Accept':'application/json',
            'Authorization' : 'Bearer $token'
          });
      var sizes = List<Size>();
      if (response.statusCode == 200) {
        var sizesJson = json.decode(response.body);
        print(sizesJson);
        for (var sizeJson in sizesJson['data']) {
          sizes.add(Size.fromJson(sizeJson));
        }
        print(sizes);
        return sizes;
      } else {
        throw Exception('Failed to get Sizes');
      }
    } catch(_){
        throw Exception(_);
    }
  }

}