import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:location/data/model/category.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class CategoryRepository{
  Future<List<Category>> fetch_all_category();
}


class ApiCategoryRepository extends CategoryRepository {

  SharedPreferences prefs;


  @override
  Future<List<Category>> fetch_all_category() async{
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');
    try{
      String citiesUrl = api_url+"categories";
      var response = await http.get(
          citiesUrl,
          headers: {
            'Accept':'application/json',
            'Authorization' : 'Bearer $token'
          });

      var categories = List<Category>();
      if (response.statusCode == 200) {
        var categoriesJson = json.decode(response.body);
        print(categoriesJson);
        for (var categoryJson in categoriesJson['data']) {
          categories.add(Category.fromJson(categoryJson));
        }
        print(categories[0].name);
        print('===========================');
        print(categories[1].brands[0].name);
        return categories;
      } else {
        throw Exception('فشل في الاتصال بالانترنت');
      }
    } catch(_){
      throw Exception("فشل في الاتصال بالانترنت");
    }
  }
}