import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:location/data/model/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_url.dart';

abstract class ProductRepository{
  Future<Product> GetProduct(String barcode);

}


class ApiProductRepository extends ProductRepository {

  SharedPreferences prefs;
  @override
  Future<Product> GetProduct(String barcode) async{
    // TODO: implement _fetch_all_cities
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');
    try{
      String zonesUrl = api_url+"products/barcode/$barcode";
      var response = await http.get(
          zonesUrl,
          headers: {
            'Accept':'application/json',
            'Authorization' : 'Bearer $token'
          });
      var product = Product();
      if (response.statusCode == 200) {
        var productsJson = json.decode(response.body);
        product = Product.fromJson(productsJson['data'][0]);
        return product;
      } else {
        throw Exception('Failed to get Product');
      }
    } catch(_){
      throw Exception(_);
    }
  }



}
