import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/data/model/locations.dart';
import 'package:location/data/repository/Locations_repo.dart';
part 'locations_event.dart';
part 'locations_state.dart';

class LocationsBloc extends Bloc<LocationsEvent, LocationsState> {
  LocationsBloc(this.repository) : super(LocationsInitial());
  final ApiLocationRepository repository;
  @override
  Stream<LocationsState> mapEventToState(
    LocationsEvent event,
  ) async* {
    // TODO: implement mapEventToState
    yield LocationsInitial();
    if (event is GetAllLocations) {
      try {
        yield LocationsLoading();
        List<Locations> locations = (await repository.fetch_all_locations());
        yield LocationsLoaded(locations);
      } catch (e) {
        print(">> Error : " + e);
        yield LocationsErrorState('فشل في الاتصال بالانترنت');
      }
    }else if(event is GetAllInitial){
      yield LocationsInitial();
    }
  }
}
