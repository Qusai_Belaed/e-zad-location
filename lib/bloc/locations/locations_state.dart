part of 'locations_bloc.dart';

abstract class LocationsState extends Equatable {
  const LocationsState();
}

class LocationsInitial extends LocationsState {
  @override
  List<Object> get props => [];
}

class LocationsLoading extends LocationsState {
  @override
  List<Object> get props => [];
}

class LocationsLoaded extends LocationsState {
  final List<Locations> location;
  const LocationsLoaded(this.location): assert(location != null);
  @override
  // TODO: implement props
  List<Object> get props => [location];
}

class LocationsErrorState extends LocationsState {
  final String message;
  const LocationsErrorState(this.message): assert(message != null);
  @override
  // TODO: implement props
  List<Object> get props => [message];
}