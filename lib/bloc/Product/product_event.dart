part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();
}


class GetProduct extends ProductEvent {

  final String barcode;

  GetProduct(this.barcode) : assert(barcode != null);

  @override
  // TODO: implement props
  List<Object> get props => [barcode];
}


class InitStateProduct extends ProductEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}