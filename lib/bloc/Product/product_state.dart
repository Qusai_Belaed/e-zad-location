part of 'product_bloc.dart';

abstract class ProductState extends Equatable {
  const ProductState();
}

class ProductInitial extends ProductState {
  @override
  List<Object> get props => [];
}

class ProductLoading extends ProductState {
  @override
  List<Object> get props => [];
}

class ProductLoaded extends ProductState {

  final Product product;

  const ProductLoaded(this.product): assert(product != null);

  @override
  // TODO: implement props
  List<Object> get props => [product];

}


class ProductErrorState extends ProductState {
  
  final String message;

  const ProductErrorState(this.message): assert(message != null);
  
  @override
  // TODO: implement props
  List<Object> get props => [message];
}