import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/bloc/store/bloc.dart';
import 'package:location/data/model/product.dart';
import 'package:location/data/repository/Zone_repo.dart';
import 'package:location/data/model/Zone.dart';
import 'package:location/data/repository/product_rep.dart';


part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {

  final ApiProductRepository repository;

  ProductBloc(this.repository): assert(repository != null), super(null);

  @override
  ProductState get initialState => ProductInitial();

  @override
  Stream<ProductState> mapEventToState(
      ProductEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is InitStateProduct){
      yield ProductInitial();
    }else if (event is GetProduct) {
      try {
          Product products = (await repository.GetProduct(event.barcode));
          print(products.name);
          yield ProductLoaded(products);
      } catch (e) {
        yield ProductErrorState(e.toString());
      }
    }
  }
}
