import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/bloc/store/bloc.dart';
import 'package:location/data/repository/Zone_repo.dart';
import 'package:location/data/model/Zone.dart';


part 'zone_event.dart';
part 'zone_state.dart';

class ZoneBloc extends Bloc<ZoneEvent, ZoneState> {

  final ApiZoneRepository repository;
  final StoreBloc storeBloc;

  ZoneBloc(this.repository, this.storeBloc): assert(repository != null), super(null);

  @override
  ZoneState get initialState => ZoneInitial();

  @override
  Stream<ZoneState> mapEventToState(
    ZoneEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if( event is AddZone){
      await repository.addNewZone(event.name , event.city_id);
      storeBloc.add(StoreInitiall());
    }else
    if (event is GetAllZones) {
      try {
          yield ZoneLoading();
          List<Zone> zones = (await repository.fetchAllZones(event.id)).cast<Zone>();
          yield ZoneLoaded(zones);
      } catch (e) {
        yield ZoneErrorState(e.toString());
      }
    }else if(event is GetZoneInitial){
      yield ZoneInitial();
    }
  }
}
