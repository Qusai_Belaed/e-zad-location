part of 'zone_bloc.dart';

abstract class ZoneState extends Equatable {
  const ZoneState();
}

class ZoneInitial extends ZoneState {
  @override
  List<Object> get props => [];
}

class ZoneLoading extends ZoneState {
  @override
  List<Object> get props => [];
}

class ZoneLoaded extends ZoneState {

  final List<Zone> zone;

  const ZoneLoaded(this.zone): assert(zone != null);

  @override
  // TODO: implement props
  List<Object> get props => [zone];

}


class ZoneErrorState extends ZoneState {
  
  final String message;

  const ZoneErrorState(this.message): assert(message != null);
  
  @override
  // TODO: implement props
  List<Object> get props => [message];
}