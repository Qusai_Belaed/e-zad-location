part of 'zone_bloc.dart';

abstract class ZoneEvent extends Equatable {
  const ZoneEvent();
}


class GetAllZones extends ZoneEvent {

  final int id;

  GetAllZones(this.id) : assert(id != null);

  @override
  // TODO: implement props
  List<Object> get props => [id];
}

class GetZoneInitial extends ZoneEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddZone extends ZoneEvent {
  final String name;
  final int city_id;

  AddZone(this.name, this.city_id) : assert(name != null , city_id != null);
  @override
  // TODO: implement props
  List<Object> get props => [name , city_id];

}