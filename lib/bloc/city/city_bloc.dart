import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/data/model/city.dart';
import 'package:location/data/repository/City_repo.dart';

part 'city_event.dart';
part 'city_state.dart';

class CityBloc extends Bloc<CityEvent, CityState> {

  final ApiCityRepository repository;

  CityBloc(this.repository): assert(repository != null), super(CityInitial());

  @override
  Stream<CityState> mapEventToState(
    CityEvent event,
  ) async* {
    // TODO: implement mapEventToState
      if (event is GetAllCities) {
      try {
        yield CityInitial();
        List<City> cities = (await repository.fetch_all_cities());
        yield CityLoaded(cities);
      } catch (e) {
        yield CityErrorState(e.toString());
      }
    }
  }
}
