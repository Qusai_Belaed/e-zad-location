part of 'city_bloc.dart';

abstract class CityState extends Equatable {
  const CityState();
}

class CityInitial extends CityState {
  @override
  List<Object> get props => [];
}

class CityLoaded extends CityState {

  final List<City> city;

  const CityLoaded(this.city): assert(city != null);

  @override
  // TODO: implement props
  List<Object> get props => [city];

}


class CityErrorState extends CityState {
  
  final String message;

  const CityErrorState(this.message): assert(message != null);
  
  @override
  // TODO: implement props
  List<Object> get props => [message];
  
}