import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/data/model/activity_type.dart';
import 'package:location/data/repository/Activity_Type_repo.dart';

part 'activity_type_event.dart';
part 'activity_type_state.dart';

class ActivityTypeBloc extends Bloc<ActivityTypeEvent, ActivityTypeState> {

  final ApiActivityTypeRepository repository;

  ActivityTypeBloc(this.repository): assert(repository != null), super(null);

  @override
  ActivityTypeState get initialState => ActivityTypeInitial();

  @override
  Stream<ActivityTypeState> mapEventToState(
    ActivityTypeEvent event,
  ) async* {
    // TODO: implement mapEventToState

    if (event is GetAllActivitiesType) {
      try {
        yield ActivityTypeInitial();
        List<ActivityType> activities = (await repository.fetch_all_activity_type());
        yield ActivityTypeLoaded(activities);
      } catch (e) {
        yield ActivityTypeErrorState(e.toString());
      }
    }
  }
}
