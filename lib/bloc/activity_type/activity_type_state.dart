part of 'activity_type_bloc.dart';

abstract class ActivityTypeState extends Equatable {
  const ActivityTypeState();
}

class ActivityTypeInitial extends ActivityTypeState {
  @override
  List<Object> get props => [];
}


class ActivityTypeLoaded extends ActivityTypeState {

  final List<ActivityType> activityType;

  const ActivityTypeLoaded(this.activityType): assert(activityType != null);

  @override
  // TODO: implement props
  List<Object> get props => [activityType];

}


class ActivityTypeErrorState extends ActivityTypeState {
  
  final String message;

  const ActivityTypeErrorState(this.message): assert(message != null);
  
  @override
  // TODO: implement props
  List<Object> get props => [message];
}