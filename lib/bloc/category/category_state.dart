part of 'category_bloc.dart';


abstract class CategoryState extends Equatable {
  const CategoryState();
}

class CategoryLoading extends CategoryState {
  @override
  List<Object> get props => [];
}

class CategoryLoaded extends CategoryState {

  final List<Category> category;

  const CategoryLoaded(this.category): assert(category != null);

  @override
  // TODO: implement props
  List<Object> get props => [category];

}


class CategoryErrorState extends CategoryState {

  final String message;

  const CategoryErrorState(this.message): assert(message != null);

  @override
  // TODO: implement props
  List<Object> get props => [message];

}