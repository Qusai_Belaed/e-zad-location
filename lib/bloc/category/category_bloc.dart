import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/data/model/category.dart';
import 'package:location/data/repository/categorey_Rep.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {

  final ApiCategoryRepository repository;

  CategoryBloc(this.repository): assert(repository != null), super(CategoryLoading());

  @override
  Stream<CategoryState> mapEventToState(
      CategoryEvent event,
      ) async* {
    if (event is GetAllCategory) {
      try {
        yield CategoryLoading();
        List<Category> Categories = (await repository.fetch_all_category());
        yield CategoryLoaded(Categories);
      } catch (e) {
        yield CategoryErrorState(e.toString());
      }
    }
  }
}
