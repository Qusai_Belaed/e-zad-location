

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/data/repository/payment_repo.dart';
import 'package:location/data/model/payment.dart';

part 'payment_event.dart';
part 'payment_state.dart';

class PaymentBloc extends Bloc<PaymentEvent, PaymentState> {


  final ApiPaymentRepository repository;

  PaymentBloc(this.repository): assert(repository != null), super(PaymentInitial());

  @override
  Stream<PaymentState> mapEventToState(
      PaymentEvent event,
      ) async* {
    // TODO: implement mapEventToState
    if (event is GetAllPayments) {
      try {
        yield PaymentInitial();
        List<Payment> payments = (await repository.fetch_all_payments());
        yield PaymentLoaded(payments);
      } catch (e) {
        yield PaymentErrorState(e.toString());
      }
    }
  }
}
