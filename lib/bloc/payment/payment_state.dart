part of 'payment_bloc.dart';


@immutable
abstract class PaymentState extends Equatable{}

class PaymentInitial extends PaymentState {
  @override
  List<Object> get props => [];
}

class PaymentLoaded extends PaymentState {

  final List<Payment> payment;

  PaymentLoaded(this.payment): assert(payment != null);

  @override
  // TODO: implement props
  List<Object> get props => [payment];

}


class PaymentErrorState extends PaymentState {

  final String message;

  PaymentErrorState(this.message): assert(message != null);

  @override
  // TODO: implement props
  List<Object> get props => [message];

}