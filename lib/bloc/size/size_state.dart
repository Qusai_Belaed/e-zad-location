part of 'size_bloc.dart';

abstract class SizeState extends Equatable {
  const SizeState();
}

class SizeInitial extends SizeState {
  @override
  List<Object> get props => [];
}

class SizeLoaded extends SizeState {

  final List<Size> size;

  const SizeLoaded(this.size): assert(size != null);

  @override
  // TODO: implement props
  List<Object> get props => [size];

}


class SizeErrorState extends SizeState {
  
  final String message;

  const SizeErrorState(this.message): assert(message != null);
  
  @override
  // TODO: implement props
  List<Object> get props => [message];
  
}