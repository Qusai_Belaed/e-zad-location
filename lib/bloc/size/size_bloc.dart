import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:location/data/model/size.dart';
import 'package:location/data/repository/size_repo.dart';

part 'size_event.dart';
part 'size_state.dart';

class SizeBloc extends Bloc<SizeEvent, SizeState> {

  final ApiSizeRepository repository;
  SizeBloc(this.repository) : super(SizeInitial());

  @override
  Stream<SizeState> mapEventToState(
    SizeEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is GetAllSizes) {
      try {
        yield SizeInitial();
        List<Size> cities = (await repository.fetchAllSizes());
        yield SizeLoaded(cities);
      } catch (e) {
        yield SizeErrorState(e.toString());
      }
    }
  }
}
