import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
// import 'package:/data/repo/store_repo.dart';
import 'package:meta/meta.dart';
import 'package:location/data/model/store.dart';

import '../../data/repository/repository.dart';

part 'bloc_event.dart';
part 'bloc_state.dart';

class StoreBloc extends Bloc<StoreEvent, StoreState> {

  final RepositoryImpl repository;

  StoreBloc(this.repository) : super(StoreInitial());


  @override
  Stream<StoreState> mapEventToState(
    StoreEvent event,
  ) async* {
    // TODO: implement mapEventToState
        if (event is StoreButtonPressed) {
        yield StoreLoadingState();
        try {
          final store = (await repository.postStore(
             event.store,
          ));
          print("store >> ======" + store);
          if(store == "success"){
            print('>> success : true');
            yield StoreLoadedState(message: "تم اظافة البيانات بنجاح");
          }else if(store == "failed"){
            print('>> failed : true');
            yield StoreErrorState(message: "فشل في اتمام العملية , الرجاء اعادة المحاولة.");
          }
        } catch (e) {
          yield StoreErrorState(message: "فشل في اتمام العملية , الرجاء اعادة المحاولة.");
        }
    }else if (event is StoreInitiall){
          yield StoreInitial();
        }
  }
}
