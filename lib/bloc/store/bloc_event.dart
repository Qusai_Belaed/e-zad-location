part of 'bloc.dart';



abstract class StoreEvent extends Equatable {
  const StoreEvent();
}

class StoreButtonPressed extends StoreEvent {
  final Store store;

  const StoreButtonPressed({
  @required this.store,
  });

  @override
  List<Object> get props => [store];

  @override
  String toString() => 'StoreButtonPressed { store: $store}';
}

class StoreInitiall extends StoreEvent {

  @override
  List<Object> get props => [];
}