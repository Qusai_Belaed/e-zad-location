part of 'bloc.dart';

abstract class StoreState extends Equatable {
  const StoreState();
}

class StoreInitial extends StoreState {
  @override
  List<Object> get props => [];
}

class StoreLoadingState extends StoreState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class StoreLoadedState extends StoreState {
  String message;

  StoreLoadedState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}

class StoreErrorState extends StoreState {

  String message;

  StoreErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}

class StoreError extends StoreState {

  String message;

  StoreError({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
