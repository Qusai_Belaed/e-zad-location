import 'dart:io';

import 'package:commons/commons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/bloc/authentication/authentication_bloc.dart';
import 'package:location/bloc/authentication/authentication_event.dart';
import 'package:location/pages/All_Locations.dart';
import 'package:location/pages/store/store.dart';
import 'package:location/pages/visit_location.dart';
import 'package:location/pages/widget/shared.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const PLAY_STORE_URL = 'https://play.google.com/store/apps/details?id=ly.elzad.location';
  static const APP_STORE_URL = 'https://apps.apple.com/us/app/alzad-store/id1510125786';

  @override
  void initState() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {
      try {
        var response = await get("http://location.elzad.ly/update.html");
        if (int.parse(packageInfo.buildNumber) < int.parse(response.body)) {
          _showVersionDialog(context);
        }
        print(response.body);

      } catch (e) {
        print("error :"+e.toString());
      }

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Color(0xFFE1E2E1),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Z-Location'),
        backgroundColor: mainColor,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.exit_to_app,
                color: Colors.white,
              ),
              onPressed: () {
                BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
              }),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            "assets/images/dashboard.png",
            height: MediaQuery.of(context).size.height * .35,
            width: double.infinity,
          ),
          SizedBox(height: MediaQuery.of(context).size.height * .05),
          SizedBox(
            width: MediaQuery.of(context).size.width * .4,
            child: RaisedButton(
              color: Colors.redAccent,
              textColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'اضافة متجر',
                      style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                    ),
                    Icon(FontAwesomeIcons.mapMarkerAlt)
                  ],
                ),
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => StorePage()));
              },
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0),
              ),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * .04),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width * .4,
                  child: RaisedButton(
                    color: mainColor,
                    textColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'زيارة موقع',
                            style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                          ),
                          Icon(FontAwesomeIcons.mapMarkedAlt)
                        ],
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> Visit_Location()));
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * .4,
                  child: RaisedButton(
                    color: mainColor,
                    textColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'كل المواقع',
                            style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                          ),
                          Icon(FontAwesomeIcons.globeAfrica)
                        ],
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => All_Locations()));
//                      warningDialog(context, 'عذرا سيتم تفعيل هذه الخدمه قريباً',
//                          textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
//      floatingActionButton: FloatingActionButton(
//        backgroundColor: mainColor,
//        onPressed: () {
//          Navigator.push(context, MaterialPageRoute(builder: (context)=> StorePage()));
//        },
//        tooltip: 'اظافة موقع',
//        child: Icon(
//          Icons.add_location,
//          color: Colors.white,
//        ),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _showVersionDialog(context) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "تحديث للتطبيق";
        String message = "يوجد تحديث جديد للتطبيق متاح عبر المتجر الان, قم بتحميله!";
        String btnLabel = "تحميل";
        return Platform.isIOS
            ? CupertinoAlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => launch(APP_STORE_URL),
                  ),
                ],
              )
            : AlertDialog(
                title: Text(title),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: () => launch(PLAY_STORE_URL),
                  ),
                ],
              );
      },
    );
  }
}
