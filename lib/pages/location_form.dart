import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/bloc/locations/locations_bloc.dart';
import 'package:location/data/model/locations.dart';
import 'package:location/pages/widget/shared.dart';

class LocationForm extends StatefulWidget {
  @override
  _LocationFormState createState() => _LocationFormState();
}

class _LocationFormState extends State<LocationForm> {
  Completer<GoogleMapController> _controller = Completer();
  double zoomVal = 5.0;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<LocationsBloc>(context).add(GetAllLocations());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationsBloc, LocationsState>(
        // ignore: missing_return
        builder: (context, state) {
          try {
            if (state is LocationsInitial) {
              return _buildIntialState();
            } else if (state is LocationsLoaded) {
              return _buildLoadedState(state.location);
            } else if (state is LocationsLoading) {
              return _buildLoadingState();
            } else if (state is LocationsErrorState) {
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: mainColor,
                  leading: IconButton(
                      icon: Icon(FontAwesomeIcons.arrowRight),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                  title: Text(
                    "كل المواقع",
                    style: textStyle(color: Colors.white),
                  ),
                  actions: [
                    IconButton(
                      icon: Icon(Icons.refresh),
                      onPressed: () {
                        BlocProvider.of<LocationsBloc>(context)
                            .add(GetAllLocations());
                      },
                    )
                  ],
                ),
                body: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.wifi),
                          Text('فشل في الاتصال بالانترنت'),
                        ],
                      ),
                      RaisedButton(
                        child: Text('إعادة المحاولة'),
                        onPressed: () {
                          BlocProvider.of<LocationsBloc>(context)
                              .add(GetAllLocations());
                        },
                      ),
                    ],
                  ),
                ),
              );
            }
            return _buildIntialState();
          } catch (_) {
            return _buildIntialState();
          }
        }
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    var marker = Marker(
//      markerId: MarkerId('ehlljkdsfkljasdf'),
//      position: LatLng(32.879139, 13.144299),
//      infoWindow: InfoWindow(title: "شمنسيبتنمشسيتب"),
//      icon: BitmapDescriptor.defaultMarkerWithHue(
//        BitmapDescriptor.hueViolet,
//      ),
//    );
//    all_markers.add(marker);
//    BlocConsumer<LocationsBloc, LocationsState>(
//      listener: (context , state){
//        if(state is LocationsErrorState){}
//      },
//      // ignore: missing_return
//      builder: (context, state) {
//        if (state is LocationsLoaded) {
//            _buildLoadedState(state.location);
//        } else if (state is LocationsErrorState) {
//          // return catch_errors();
//        } else if (state is LocationsInitial){
//          return Scaffold(
//            appBar: AppBar(
//              leading: IconButton(
//                  icon: Icon(FontAwesomeIcons.arrowRight),
//                  onPressed: () {
//                    Navigator.pop(context);
//                  }),
//              title: Text(
//                "كل المواقع",
//                style: textStyle(color: Colors.white),
//              ),
//              actions: [
//                IconButton(
//                  icon: Icon(Icons.refresh),
//                  onPressed: () {
//                    BlocProvider.of<LocationsBloc>(context)
//                        .add(GetAllLocations());
//                  },
//                )
//              ],
//            ),
//            body: Center(
//              child: CircularProgressIndicator(),
//            ),
//          );
//        }
//      },
//    );
//  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

//  Widget _zoomminusfunction() {
//    return Align(
//      alignment: Alignment.topLeft,
//      child: IconButton(
//          icon: Icon(FontAwesomeIcons.searchMinus,color:Color(0xff6200ee)),
//          onPressed: () {
//            zoomVal--;
//            _minus( zoomVal);
//          }),
//    );
//  }
//  Widget _zoomplusfunction() {
//    return Align(
//      alignment: Alignment.topRight,
//      child: IconButton(
//          icon: Icon(FontAwesomeIcons.searchPlus,color:Color(0xff6200ee)),
//          onPressed: () {
//            zoomVal++;
//            _plus(zoomVal);
//          }),
//    );
//  }
//  Widget _buildContainer() {
//    return Align(
//      alignment: Alignment.bottomLeft,
//      child: Container(
//        margin: EdgeInsets.symmetric(vertical: 20.0),
//        height: 150.0,
//        child: ListView(
//          scrollDirection: Axis.horizontal,
//          children: <Widget>[
//            SizedBox(width: 10.0),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: _boxes(
//                  "https://lh5.googleusercontent.com/p/AF1QipO3VPL9m-b355xWeg4MXmOQTauFAEkavSluTtJU=w225-h160-k-no",
//                  40.738380, -73.988426,"Gramercy Tavern"),
//            ),
//            SizedBox(width: 10.0),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: _boxes(
//                  "https://lh5.googleusercontent.com/p/AF1QipMKRN-1zTYMUVPrH-CcKzfTo6Nai7wdL7D8PMkt=w340-h160-k-no",
//                  40.761421, -73.981667,"Le Bernardin"),
//            ),
//            SizedBox(width: 10.0),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: _boxes(
//                  "https://images.unsplash.com/photo-1504940892017-d23b9053d5d4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
//                  40.732128, -73.999619,"Blue Hill"),
//            ),
//          ],
//        ),
//      ),
//    );
//  }

//  Widget _boxes(String _image, double lat,double long,String restaurantName) {
//    return  GestureDetector(
//      onTap: () {
//        _gotoLocation(lat,long);
//      },
//      child:Container(
//        child: new FittedBox(
//          child: Material(
//              color: Colors.white,
//              elevation: 14.0,
//              borderRadius: BorderRadius.circular(24.0),
//              shadowColor: Color(0x802196F3),
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  Container(
//                    width: 180,
//                    height: 200,
//                    child: ClipRRect(
//                      borderRadius: new BorderRadius.circular(24.0),
//                      child: Image(
//                        fit: BoxFit.fill,
//                        image: NetworkImage(_image),
//                      ),
//                    ),),
//                  Container(
//                    child: Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: myDetailsContainer1(restaurantName),
//                    ),
//                  ),
//
//                ],)
//          ),
//        ),
//      ),
//    );
//  }

//  Widget myDetailsContainer1(String restaurantName) {
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//      children: <Widget>[
//        Padding(
//          padding: const EdgeInsets.only(left: 8.0),
//          child: Container(
//              child: Text(restaurantName,
//                style: TextStyle(
//                    color: Color(0xff6200ee),
//                    fontSize: 24.0,
//                    fontWeight: FontWeight.bold),
//              )),
//        ),
//        SizedBox(height:5.0),
//        Container(
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//              children: <Widget>[
//                Container(
//                    child: Text(
//                      "4.1",
//                      style: TextStyle(
//                        color: Colors.black54,
//                        fontSize: 18.0,
//                      ),
//                    )),
//                Container(
//                  child: Icon(
//                    FontAwesomeIcons.solidStar,
//                    color: Colors.amber,
//                    size: 15.0,
//                  ),
//                ),
//                Container(
//                  child: Icon(
//                    FontAwesomeIcons.solidStar,
//                    color: Colors.amber,
//                    size: 15.0,
//                  ),
//                ),
//                Container(
//                  child: Icon(
//                    FontAwesomeIcons.solidStar,
//                    color: Colors.amber,
//                    size: 15.0,
//                  ),
//                ),
//                Container(
//                  child: Icon(
//                    FontAwesomeIcons.solidStar,
//                    color: Colors.amber,
//                    size: 15.0,
//                  ),
//                ),
//                Container(
//                  child: Icon(
//                    FontAwesomeIcons.solidStarHalf,
//                    color: Colors.amber,
//                    size: 15.0,
//                  ),
//                ),
//                Container(
//                    child: Text(
//                      "(946)",
//                      style: TextStyle(
//                        color: Colors.black54,
//                        fontSize: 18.0,
//                      ),
//                    )),
//              ],
//            )),
//        SizedBox(height:5.0),
//        Container(
//            child: Text(
//              "American \u00B7 \u0024\u0024 \u00B7 1.6 mi",
//              style: TextStyle(
//                color: Colors.black54,
//                fontSize: 18.0,
//              ),
//            )),
//        SizedBox(height:5.0),
//        Container(
//            child: Text(
//              "Closed \u00B7 Opens 17:00 Thu",
//              style: TextStyle(
//                  color: Colors.black54,
//                  fontSize: 18.0,
//                  fontWeight: FontWeight.bold),
//            )),
//      ],
//    );
//  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
          mapType: MapType.hybrid,
          initialCameraPosition:
              CameraPosition(target: LatLng(32.885653, 13.183515), zoom: 12),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          markers: all_markers),
    );
  }

  Future<void> _gotoLocation(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 15,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }

  Widget _buildLoadedState(List<Locations> location){
    for (int i = 0; i < location.length; i++) {
      var marker = Marker(
        markerId: MarkerId(location[i].name),
        position: LatLng(double.parse(location[i].lat.toStringAsFixed(10)), double.parse(location[i].lng.toStringAsFixed(10))),
        infoWindow: InfoWindow(title: location[i].name),
        icon: BitmapDescriptor.defaultMarkerWithHue(
          BitmapDescriptor.hueGreen,
        )
      );
      all_markers.add(marker);
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        leading: IconButton(
            icon: Icon(FontAwesomeIcons.arrowRight),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "كل المواقع",
          style: textStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              BlocProvider.of<LocationsBloc>(context).add(GetAllLocations());
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          _buildGoogleMap(context),
          _buildTotalStore(location.length)
        ],
      ),
    );
  }

  var all_markers = [
    Marker(
      markerId: MarkerId('Elzad Company'),
      position: LatLng(32.879139, 13.143299),
      infoWindow: InfoWindow(title: 'شركة الزاد للتجارة و التسويق'),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        BitmapDescriptor.hueGreen,
      ),
    ),
  ].toSet();

  Widget _buildIntialState() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        leading: IconButton(
            icon: Icon(FontAwesomeIcons.arrowRight),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "كل المواقع",
          style: textStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              BlocProvider.of<LocationsBloc>(context)
                  .add(GetAllLocations());
            },
          )
        ],
      ),
      body: _buildGoogleMap(context),
    );
  }

  Widget _buildTotalStore(int total) {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(FontAwesomeIcons.checkSquare, color: mainColor,),
              SizedBox(width: 8),
              Text("تهانينا لقد قمت بتسجيل $total متجر" , style: textStyle(color: mainColor),)
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLoadingState() {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        leading: IconButton(
            icon: Icon(FontAwesomeIcons.arrowRight),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "كل المواقع",
          style: textStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              BlocProvider.of<LocationsBloc>(context)
                  .add(GetAllLocations());
            },
          )
        ],
      ),
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
