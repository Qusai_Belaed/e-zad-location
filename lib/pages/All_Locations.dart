import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/bloc/locations/locations_bloc.dart';
import 'package:location/data/repository/Locations_repo.dart';
import 'package:location/pages/location_form.dart';

class All_Locations extends StatefulWidget {
  @override
  _All_LocationsState createState() => _All_LocationsState();
}

class _All_LocationsState extends State<All_Locations> {
  @override
  Widget build(BuildContext context) {
      return BlocProvider(
        create: (context) => LocationsBloc(ApiLocationRepository()),
        child: LocationForm()
      );
  }
}

