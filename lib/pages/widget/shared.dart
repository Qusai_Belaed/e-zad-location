import 'package:commons/commons.dart';
import 'package:flutter/material.dart';

AppBar mainAppBar({String title = 'Z-Location' , Color color = Colors.black}){
  return AppBar(
    centerTitle: true,
    title: Text(title , style : textStyle(color: color)),
    backgroundColor: mainColor,
  );
}

TextStyle textStyle({double fontSize = 18, Color color = Colors.black}){
  return TextStyle(fontFamily: "Cairo", fontSize: fontSize , color: color);
}

const Color mainColor = const Color(0xff085e55);


Widget loading(BuildContext context){
  return loadingScreen(
    context,
    duration: Duration(
      seconds: 5,
    ),
    loadingType: LoadingType.JUMPING,
  );
}

Widget progressLoading(BuildContext context){
  return Dialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(16.0),
    ),
    elevation: 0.5,
    backgroundColor:
    Theme.of(context).dialogBackgroundColor,
    child: Padding(
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment:
        MainAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: EdgeInsets.all(8),
              child: CircularProgressIndicator()),
          Text(
            "الرجاء الانتظار قليلا",
            textAlign: TextAlign.right,
          ),
        ],
      ),
    ),
  );
}