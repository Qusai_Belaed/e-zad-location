
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:location/data/model/brand.dart';
import 'package:location/pages/scan/Before_product_screen.dart';
import 'package:location/pages/scan/scan.dart';

class BrandGridWidget extends StatefulWidget {
  final List<Brand> brands;

  const BrandGridWidget({
    Key key,
    this.brands,
  }) : super(key: key);

  @override
  _BrandGridWidget createState() => _BrandGridWidget();
}

class _BrandGridWidget extends State<BrandGridWidget> {

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.symmetric(vertical: 15),
      shrinkWrap: true,
      primary: false,
      itemCount: widget.brands.length,
      separatorBuilder: (context, index) {
        return SizedBox(height: 3);
      },
      itemBuilder: (context, index) {
        Brand brand = widget.brands.elementAt(index);
        return InkWell(
           onTap: () {
             Navigator.push(context, MaterialPageRoute(builder: (context)=> BeforeProductScreen()));
           },
          child: Container(

            margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
            decoration: BoxDecoration(
              color:Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).focusColor.withOpacity(0.1),
                    blurRadius: 15,
                    offset: Offset(0, 5)),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                // Image of the card
                Stack(
                  fit: StackFit.loose,
                  alignment: AlignmentDirectional.bottomStart,
                  children: <Widget>[
                    Hero(
                      tag: brand.id,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        child: CachedNetworkImage(
                          height: 150,
                          width: double.infinity,
                          fit: BoxFit.contain,
                          imageUrl: brand.image.url,
                          placeholder: (context, url) => Image.asset(
                            'assets/images/loading.gif',
                            fit: BoxFit.cover,
                            width: double.infinity,
                            height: 150,
                          ),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                      ),
                    ),

                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              brand.name,
                              overflow: TextOverflow.fade,
                              softWrap: false,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),

                          ],
                        ),
                      ),

                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },

    );
  }
}
