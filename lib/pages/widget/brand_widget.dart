import 'package:flutter/material.dart';
import 'package:location/data/model/category.dart';

import 'BrandGridWidget.dart';

class BrandWidget extends StatefulWidget {
  final Category category;
  final Function(num id) onTap;

  const BrandWidget(this.category, {this.onTap});

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<BrandWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left:5.0,right: 5.0,bottom: 5.0),
          child: widget.category.brands.isEmpty
              ? Text('no data')
              : SingleChildScrollView(
                  child: Wrap(
                    children: <Widget>[
                      BrandGridWidget(brands: widget.category.brands),
                    ],
                  ),
                ),
        ),
      ],
    );
  }
}
