import 'package:flutter/material.dart';
import 'package:location/data/model/category.dart';

import 'brand_widget.dart';

class CategoryWidget extends StatefulWidget {
  final Category category;
  final Function(num id) onTap;

  const CategoryWidget(this.category, {this.onTap});

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Theme.of(context).primaryColor, width: 1)),
              child: Align(
                alignment: Alignment.center,
                child: Text(widget.category.name),
              ),
            ),
            onTap: (){
              setState(() {
              //return  BrandWidget(widget.category);
              });
            },
          ),
        ),
      ],
    );
  }
}
