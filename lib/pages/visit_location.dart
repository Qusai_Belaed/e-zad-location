import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/pages/category/tap.dart';
import 'package:location/pages/widget/shared.dart';

import 'category/Before_category_screen.dart';

class Visit_Location extends StatefulWidget {
  @override
  _Visit_LocationState createState() => _Visit_LocationState();
}

class _Visit_LocationState extends State<Visit_Location> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  String value , QR = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(title: "زيارة موقع" , color: Colors.white),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .5,
              child: RaisedButton(
                color: Colors.redAccent,
                textColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(right: 4 , left : 4 , top : 8 , bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text('قراءة  QR', style: TextStyle(fontFamily: "Cairo", fontSize: 13),), Icon(FontAwesomeIcons.qrcode)],
                  ),
                ),
                onPressed: () {
                  QR_Scanner();
                },
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
              ),
            ),
          ),
          Text(value != null ? value : ""),
          Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .8,
              child: RaisedButton(
                color: Colors.teal,
                textColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(right: 4 , left : 4 , top : 8 , bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text('البداء في عملية الحصر', style: TextStyle(fontFamily: "Cairo", fontSize: 13),), Icon(Icons.visibility)],
                  ),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> BeforeCategoryScreen()));
                },
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


  QR_Scanner() async{
    QR = await FlutterBarcodeScanner.scanBarcode("#004297", "Cancel", true , ScanMode.QR);

    setState(() {
      value = QR;
    });
  }
}
