import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/bloc/activity_type/activity_type_bloc.dart';
import 'package:location/bloc/city/city_bloc.dart';
import 'package:location/bloc/payment/payment_bloc.dart';
import 'package:location/bloc/size/size_bloc.dart';
import 'package:location/bloc/store/bloc.dart';
import 'package:location/bloc/zone/zone_bloc.dart';
import 'package:location/data/repository/Activity_Type_repo.dart';
import 'package:location/data/repository/City_repo.dart';
import 'package:location/data/repository/Zone_repo.dart';
import 'package:location/data/repository/payment_repo.dart';
import 'package:location/data/repository/repository.dart';
import 'package:location/data/repository/size_repo.dart';
import 'package:location/pages/widget/shared.dart';
import '../store/store_form.dart';

class StorePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(title: "اضافة متجر" , color: Colors.white),
        body: MultiBlocProvider(
          providers: [
              BlocProvider<CityBloc>(
                create: (context) => CityBloc(ApiCityRepository()),
              ),
              BlocProvider<ZoneBloc>(
                create: (context) => ZoneBloc(ApiZoneRepository() , StoreBloc(RepositoryImpl())),
              ),
              BlocProvider<ActivityTypeBloc>(
                create: (context) => ActivityTypeBloc(ApiActivityTypeRepository()),
              ),
              BlocProvider<PaymentBloc>(
                create: (context) => PaymentBloc(ApiPaymentRepository()),
              ),
              BlocProvider<SizeBloc>(
                create: (context) => SizeBloc(ApiSizeRepository()),
              ),
          ],
          child: Add_Location(),
        )
    );
  }
}