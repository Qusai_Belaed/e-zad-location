import 'package:commons/commons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:location/bloc/activity_type/activity_type_bloc.dart';
import 'package:location/bloc/city/city_bloc.dart';
import 'package:location/bloc/payment/payment_bloc.dart';
import 'package:location/bloc/size/size_bloc.dart';
import 'package:location/bloc/store/bloc.dart';
import 'package:location/bloc/zone/zone_bloc.dart';
import 'package:location/data/model/Zone.dart';
import 'package:location/data/model/activity_type.dart';
import 'package:location/data/model/city.dart';
import 'package:location/data/model/payment.dart';
import 'package:location/data/model/size.dart';
import 'package:location/data/model/store.dart';
import 'package:location/data/model/store_status.dart';
import 'package:location/pages/widget/shared.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Add_Location extends StatefulWidget {
  @override
  _Add_LocationState createState() => _Add_LocationState();
}

class _Add_LocationState extends State<Add_Location> {
  LocationResult _pickedLocation;
  Store store = Store();
  bool _lights = false;
  final _nameController = TextEditingController();
  final _ownerController = TextEditingController();
  final _sellerController = TextEditingController();
  final _addressController = TextEditingController();
  final _phone2Controller = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _sizeController = TextEditingController();
  final _counter_noController = TextEditingController();

  CityBloc cityBloc;
  ZoneBloc zoneBloc;
  SizeBloc sizeBloc;
  PaymentBloc paymentBloc;
  ActivityTypeBloc activityBloc;
  List<Asset> images = List<Asset>();
  List<DropdownMenuItem<City>> _dropdownMenuCityItems;
  City _selectedCity;
  List<DropdownMenuItem<Size>> _dropdownMenuSizeItems;
  Size _selectedSize;
  List<DropdownMenuItem<Zone>> _dropdownMenuZoneItems;
  Zone _selectedZone;
  List<DropdownMenuItem<ActivityType>> _dropdownMenuActivityItems;
  ActivityType _selectedActivity;
  List<StoreStatus> _storeStatus = StoreStatus.getCompanies();
  StoreStatus _selectedStoreStatus;
  MultipartRequest request;
  SharedPreferences prefs;
  List<MultipartFile> pictures;
  String value = "";
  List<DropdownMenuItem<String>> menuitems = List();
  bool disabledropdown = true;
  List _myActivities;
  List<MultiSelectItem> items;
  int _is_store;
  String _text = "initial";
  TextEditingController _c;

//  LatLng initialPlace;

  final _formState = GlobalKey<FormState>();
  final phoneValidator = MultiValidator([
    RequiredValidator(errorText: 'يرجاء ادخال رقم الهاتف'),
    MinLengthValidator(10, errorText: 'يجب أن تتكون رقم الهاتف من 10 أرقام على الأقل'),
  ]);

  @override
  void initState() {
    cityBloc = BlocProvider.of<CityBloc>(context);
    zoneBloc = BlocProvider.of<ZoneBloc>(context);
    sizeBloc = BlocProvider.of<SizeBloc>(context);
    activityBloc = BlocProvider.of<ActivityTypeBloc>(context);
    paymentBloc = BlocProvider.of<PaymentBloc>(context);
    cityBloc.add(GetAllCities());
    activityBloc.add(GetAllActivitiesType());
    zoneBloc.add(GetZoneInitial());
    sizeBloc.add(GetAllSizes());
    paymentBloc.add(GetAllPayments());
    _myActivities = List();
    _myActivities.add(1);
    _c = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _nameController.dispose();
    _ownerController.dispose();
    _sellerController.dispose();
    _addressController.dispose();
    _phoneController.dispose();
    _phone2Controller.dispose();
    _emailController.dispose();
    _sizeController.dispose();
    _counter_noController.dispose();
    _c.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<StoreBloc, StoreState>(
      listener: (context, state) {
        if (state is StoreLoadedState) {
          successDialog(context, "تمت اتمام العملية بنجاح", neutralText: 'حسنا', neutralAction: () {
            Navigator.pop(context);
          }, title: "تهانينا");
        } else if (state is StoreErrorState) {
          errorDialog(context, "فشل في اتمام العملية , الرجاء اعادة المحاولة.", title: "خطأ", neutralText: "حسنا");
        }
      },
      child: BlocBuilder<StoreBloc, StoreState>(
        builder: (context, state) {
          return Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Form(
                    key: _formState,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 20,
                        ),
                        DropDownList(),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _nameController,
                          style: textStyle(fontSize: 16),
                          validator: RequiredValidator(errorText: 'يرجاء ادخال اسم المتجر'),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                            labelText: 'اسم المتجر',
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _ownerController,
                          style: textStyle(fontSize: 16),
                          validator: RequiredValidator(errorText: 'يرجاء ادخال اسم المالك'),
                          decoration: InputDecoration(
                            labelText: 'اسم المالك',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _sellerController,
                          style: textStyle(fontSize: 16),
                          validator: RequiredValidator(errorText: 'يرجاد ادخال اسم مدير المشتريات'),
                          decoration: InputDecoration(
                            labelText: 'اسم مدير المشتريات',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _phoneController,
                          style: textStyle(fontSize: 16),
                          decoration: InputDecoration(
                            labelText: 'رقم الهاتف',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                          validator: phoneValidator,
                          maxLength: 10,
                          keyboardType: TextInputType.phone,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _phone2Controller,
                          style: textStyle(fontSize: 16),
                          decoration: InputDecoration(
                            labelText: 'رقم هاتف 2',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                          maxLength: 10,
                          keyboardType: TextInputType.phone,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _emailController,
                          style: textStyle(fontSize: 16),
                          decoration: InputDecoration(
                            labelText: 'البريد الالكتروني',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _counter_noController,
                          style: textStyle(fontSize: 16),
                          maxLength: 2,
                          decoration: InputDecoration(
                            labelText: 'عدد الكاشير \"كاصة\"',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                          validator: RequiredValidator(errorText: 'يرجاء ادخال عدد نقاط البيع'),
                          keyboardType: TextInputType.phone,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        BlocConsumer<PaymentBloc, PaymentState>(
                          listener: (context, state) {
                            if (state is PaymentInitial) {
                              return Container(
                                child: Text('Hello'),
                              );
                            }
                          },
                          // ignore: missing_return
                          builder: (context, state) {
                            if (state is PaymentInitial) {
                              return Column(
                                children: [
                                  Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              );
                            } else if (state is PaymentLoaded) {
                              return DisplayItems(state.payment);
                            } else if (state is PaymentErrorState) {
                              return catch_errors();
                            }
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: _addressController,
                          style: textStyle(fontSize: 16),
                          validator: RequiredValidator(errorText: 'يرجاء ادخال العنوان'),
                          decoration: InputDecoration(
                            labelText: 'اقرب نقطه داله',
                            border: OutlineInputBorder(borderSide: BorderSide(color: mainColor)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SwitchListTile(
                          title: Text('هل لديه مخزن ؟', style: textStyle()),
                          value: _lights,
                          onChanged: (bool value) {
                            setState(() {
                              _lights = value;
                            });
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            SizedBox(
                              width: MediaQuery.of(context).size.width * .4,
                              child: RaisedButton(
                                color: mainColor,
                                textColor: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                                  child: Text(
                                    "اضافة صور",
                                    style: textStyle(fontSize: 16, color: Colors.white),
                                  ),
                                ),
                                onPressed: loadAssets,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * .4,
                              child: RaisedButton(
                                color: mainColor,
                                textColor: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                                  child: Text(
                                    "تحديد الموقع",
                                    style: textStyle(fontSize: 16, color: Colors.white),
                                  ),
                                ),
                                onPressed: () async {
                                  LocationResult result = await showLocationPicker(context, "AIzaSyDKNsqVQn-1t08BQRIKvIvumVLYnjEQ7J0",
                                      requiredGPS: false,
                                      initialCenter: LatLng(32.884089, 13.182328),
                                      automaticallyAnimateToCurrentLocation: true,
                                      myLocationButtonEnabled: true,
                                      layersButtonEnabled: true);
                                  setState(() => _pickedLocation = result);
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        state is StoreLoadingState ? loading(context) : SizedBox(),
                        state is StoreLoadingState
                            ? SizedBox(
                                height: 10,
                              )
                            : SizedBox(),
                        GestureDetector(
                          onTap: state is! StoreLoadingState ? _onStoreButtonPressed : null,
                          child: state is! StoreLoadingState
                              ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(5)),
                                      boxShadow: <BoxShadow>[BoxShadow(color: Colors.grey.shade200, offset: Offset(2, 4), blurRadius: 5, spreadRadius: 2)],
                                      gradient: LinearGradient(begin: Alignment.centerLeft, end: Alignment.centerRight, colors: [mainColor, Colors.teal])),
                                  child: Text(
                                    'اتمام العملية',
                                    style: textStyle(fontSize: 20, color: Colors.white),
                                  ),
                                )
                              : progressLoading(context),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * .055),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget DisplayItems(List<Payment> payments) {
    items = List();
    payments.forEach((element) {
      items.add(MultiSelectItem(element.id, element.name));
    });
    return MultiSelectField(
      title: "طرق الدفع",
      buttonText: "الرجاء اختيار طرق الدفع",
      items: items,
      dialogType: MultiSelectDialogType.LIST,
      initialValue: _myActivities,
      buttonIcon: Icon(Icons.arrow_drop_down),
//      textStyle: TextStyle(color: mainColor),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Colors.grey,
        ),
      ),
      onConfirm: (results) {
        print(">> results : " + results.toString());
        _myActivities = List();
        setState(() {
          results.forEach((element) {
            _myActivities.add(element);
          });
        });
        print(">> myActivities : " + _myActivities.toString());
      },
    );
  }

  _onStoreButtonPressed() {
    if (_formState.currentState.validate()) {
      if (_myActivities != null && !_myActivities.isEmpty) {
        if (pictures != null && pictures != [] && !pictures.isEmpty) {
          if (_pickedLocation != null && _pickedLocation != "") {
            print(">> myActivity after data pushed : " + _myActivities.toString());
            store.name = _nameController.text.trim();
            store.owner = _ownerController.text.trim();
            store.seller = _sellerController.text.trim();
            store.address = _addressController.text.trim();
            store.lat = double.parse(_pickedLocation.latLng.latitude.toString().substring(0, 10).trim().toString());
            store.lng = double.parse(_pickedLocation.latLng.longitude.toString().substring(0, 10).trim().toString());
            store.plusCode = "plusCode";
            store.phone = _phoneController.text.trim();
            if (_phone2Controller.text.isEmpty) {
              store.phone2 = '0';
            } else {
              store.phone2 = _phone2Controller.text.trim();
            }
            if (_emailController.text.isEmpty) {
              store.email = "";
            } else {
              store.email = _emailController.text.trim();
            }
            store.counterNo = int.parse(_counter_noController.text.trim());
            store.pictures = pictures;
            store.storeStute = 0;
            if (_lights) {
              _is_store = 1;
            } else {
              _is_store = 0;
            }
            store.isStore = _is_store;
            store.payment = _myActivities;
            store.cityId = _selectedCity.id;
            store.zoneId = _selectedZone.id;
            store.sizeId = _selectedSize.id;
            store.activityTypeId = _selectedActivity.id;
            BlocProvider.of<StoreBloc>(context).add(
              StoreButtonPressed(
                store: store,
              ),
            );
          } else {
            warningDialog(context, 'الرجاء تحديد الموقع', textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
          }
        } else {
          warningDialog(context, 'الرجاء اختيار صورة', textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
        }
      } else {
        warningDialog(context, "الرجاء اختيار طرق الدفع", textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
      }
    } else {
      warningDialog(context, 'الرجاء تعبئة الحقول الفارغه.', textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
    }
  }

  Widget DropDownList() {
    try {
      return Column(
        children: [
          BlocConsumer<CityBloc, CityState>(
            listener: (context, state) {
              if (state is CityInitial) {
                return Container(
                  child: Text('Hello'),
                );
              } else if (state is CityErrorState) {
                warningDialog(context, state.message.split(":")[1], textAlign: TextAlign.start, title: "! تنبيه", neutralText: "حسنا");
              }
            },
            builder: (context, state) {
              if (state is CityInitial) {
                return Column(
                  children: [
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              } else if (state is CityLoaded) {
                try {
                  _dropdownMenuCityItems = buildDropdownMenuCityItems(state.city);
                  return buildCityList(state.city);
                } catch (_) {
                  return buildErrorUi(_);
                }
              } else if (state is CityErrorState) {
                return buildErrorUi(state.message);
              }
            },
          ),
          BlocConsumer<ZoneBloc, ZoneState>(
            listener: (context, state) {},
            builder: (context, state) {
              if (state is ZoneInitial) {
                this._dropdownMenuZoneItems = buildDropdownMenuZoneItems([]);
                _selectedZone = null;
                return Container(
                  child: Text(''),
                );
              } else if (state is ZoneLoading) {
                return Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              } else if (state is ZoneLoaded) {
                try {
//                  state.zone.removeAt(0);
                  _dropdownMenuZoneItems = buildDropdownMenuZoneItems(state.zone);
                } catch (_) {
                  buildErrorUi(_);
                }
                return Column(
                  children: [
                    SizedBox(height: 20),
                    buildZoneList(state.zone),
                    SizedBox(height: 10),
                    RaisedButton(
                      child: Row(
                        children: <Widget>[
                          Text('اضافة منطقة', style: textStyle(color: mainColor)),
                          Icon(
                            Icons.add,
                            color: mainColor,
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                      ),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return Dialog(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)), //this right here
                                child: Container(
                                  height: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        TextField(
                                          controller: _c,
                                          decoration: InputDecoration(border: InputBorder.none, hintText: 'اضف اسم المنطقة هنا'),
                                          style: textStyle(),
                                          textAlign: TextAlign.right,
                                        ),
                                        SizedBox(
                                          width: 320.0,
                                          child: RaisedButton(
                                            onPressed: () {
                                              print(_c.text.trim().toString());
                                              zoneBloc.add(AddZone(_c.text.trim(), _selectedCity.id));
                                              zoneBloc.add(GetAllZones(_selectedCity.id));
                                              Navigator.pop(context);
                                            },
                                            child: Text(
                                              "اضافة",
                                              style: textStyle(color: Colors.white),
                                            ),
                                            color: const Color(0xFF1BC0C5),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            });
                      },
                    )
                  ],
                );
              } else if (state is ZoneErrorState) {
                return buildErrorUi(state.message);
              }
            },
          ),
          BlocConsumer<ActivityTypeBloc, ActivityTypeState>(
            listener: (context, state) {
              if (state is ActivityTypeErrorState) {}
            },
            builder: (context, state) {
              if (state is ActivityTypeInitial) {
                return Column(
                  children: [
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              } else if (state is ActivityTypeLoaded) {
                try {
                  _dropdownMenuActivityItems = buildDropdownMenuActivityItems(state.activityType);
                  return Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      buildActivityList(state.activityType),
                      SizedBox(height: 20),
                    ],
                  );
                } catch (_) {
                  return catch_errors();
                }
              } else if (state is ActivityTypeErrorState) {
                return catch_errors();
              }
            },
          ),
          BlocConsumer<SizeBloc, SizeState>(
            listener: (context, state) {
              if (state is SizeErrorState) {}
            },
            builder: (context, state) {
              if (state is SizeInitial) {
                return Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                );
              } else if (state is SizeLoaded) {
                try {
//                  state.size.removeAt(0);
                  _dropdownMenuSizeItems = buildDropdownMenuSizeItems(state.size);
                  return buildSizeList(state.size);
                } catch (_) {
                  return catch_errors();
                }
              } else if (state is SizeErrorState) {
                return catch_errors();
              }
            },
          ),
        ],
      );
    } catch (_) {
      print(_);
      return Container();
    }
  }

  Widget catch_errors() {
    return Container();
  }

  List<DropdownMenuItem<StoreStatus>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<StoreStatus>> items = List();
    for (StoreStatus store_status in _storeStatus) {
      items.add(
        DropdownMenuItem(
          value: store_status,
          child: Text(store_status.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(StoreStatus selectedCompany) {
    setState(() {
      this._selectedStoreStatus = selectedCompany;
      print(_selectedStoreStatus.id);
    });
  }

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
      pictures = List();
    });
    List<Asset> resultList;
    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 5, enableCamera: true);
    } on Exception catch (e) {
      print(e.toString());
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
    });

    resultList.forEach((asset) {
      asset.getByteData(quality: 25).then((byteData) {
        print('by ' + byteData.toString());
        List<int> imageData = byteData.buffer.asUint8List();
        MultipartFile multipartFile = MultipartFile.fromBytes(
          'Picture_url[]',
          imageData,
          filename: asset.name,
          contentType: MediaType("image", "jpg"),
        );
        pictures.add(multipartFile);
      });
    });
  }

  onChangeDropdownCityItem(City selectedCity) {
    setState(() {
      print(selectedCity.id);
      zoneBloc.add(GetAllZones(selectedCity.id));
      this._dropdownMenuZoneItems = buildDropdownMenuZoneItems([]);
      _selectedZone = null;
      _selectedCity = selectedCity;
    });
  }

  onChangeDropdownZoneItem(Zone selectedZone) {
    setState(() {
      _selectedZone = selectedZone;
    });
  }

  onChangeDropdownActivityItem(ActivityType selectedActivity) {
    setState(() {
      _selectedActivity = selectedActivity;
    });
  }

  onChangeDropdownSizeItem(Size selectedSize) {
    setState(() {
      _selectedSize = selectedSize;
    });
  }

  List<DropdownMenuItem<City>> buildDropdownMenuCityItems(List cities) {
    List<DropdownMenuItem<City>> items = List();
    for (City city in cities) {
      items.add(
        DropdownMenuItem(
          value: city,
          child: Text(city.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Zone>> buildDropdownMenuZoneItems(List zones) {
    List<DropdownMenuItem<Zone>> items = List();
    for (Zone zone in zones) {
      items.add(
        DropdownMenuItem(
          value: zone,
          child: Text(zone.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<ActivityType>> buildDropdownMenuActivityItems(List activities) {
    List<DropdownMenuItem<ActivityType>> items = List();
    for (ActivityType activity in activities) {
      items.add(
        DropdownMenuItem(
          value: activity,
          child: Text(activity.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Size>> buildDropdownMenuSizeItems(List sizes) {
    List<DropdownMenuItem<Size>> items = List();
    for (Size size in sizes) {
      items.add(
        DropdownMenuItem(
          value: size,
          child: Text(size.name),
        ),
      );
    }
    return items;
  }

  Widget buildErrorUi(String message) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.wifi),
              Text('فشل في الاتصال بالانترنت'),
            ],
          ),
          RaisedButton(
            child: Text('إعادة المحاولة'),
            onPressed: () {
              cityBloc.add(GetAllCities());
              activityBloc.add(GetAllActivitiesType());
              zoneBloc.add(GetZoneInitial());
              sizeBloc.add(GetAllSizes());
              paymentBloc.add(GetAllPayments());
            },
          ),
        ],
      ),
    );
  }

  Widget buildCityList(cities) {
    return DropdownButtonFormField(
      value: _selectedCity,
      items: _dropdownMenuCityItems,
      isExpanded: true,
      validator: (value) => value == null ? 'االرجاء اختيار المدينة' : null,
      hint: Text('الرجاء اختيار المدينة'),
      onChanged: onChangeDropdownCityItem,
    );
  }

  Widget buildZoneList(zones) {
    return DropdownButtonFormField(
      value: _selectedZone,
      items: _dropdownMenuZoneItems,
      validator: (value) => value == null ? 'االرجاء اختيار المنطقة' : null,
      isExpanded: true,
      hint: Text('الرجاء اختيار المنظقة'),
      onChanged: onChangeDropdownZoneItem,
    );
  }

  Widget buildActivityList(activities) {
    return DropdownButtonFormField(
      value: _selectedActivity,
      items: _dropdownMenuActivityItems,
      validator: (value) => value == null ? 'االرجاء اختيار نوع النشاط' : null,
      isExpanded: true,
      hint: Text('الرجاء اختيار نوع النشاط'),
      onChanged: onChangeDropdownActivityItem,
    );
  }

  Widget buildSizeList(sizes) {
    return DropdownButtonFormField(
      value: _selectedSize,
      items: _dropdownMenuSizeItems,
      validator: (value) => value == null ? 'االرجاء اختيار حجم المتجر' : null,
      isExpanded: true,
      hint: Text('الرجاء اختيار حجم المتجر'),
      onChanged: onChangeDropdownSizeItem,
    );
  }
}
