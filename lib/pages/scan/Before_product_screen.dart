import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/bloc/Product/product_bloc.dart';
import 'package:location/data/repository/product_rep.dart';
import 'package:location/pages/scan/scan.dart';


class BeforeProductScreen extends StatefulWidget {
  @override
  _BeforeProductScreenState createState() => _BeforeProductScreenState();
}

class _BeforeProductScreenState extends State<BeforeProductScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => ProductBloc(ApiProductRepository()),
      child: Scan(),
    );
  }
}
