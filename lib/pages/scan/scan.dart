import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/bloc/Product/product_bloc.dart';
import 'package:location/data/model/product.dart';
import 'package:location/pages/widget/shared.dart';

import '../../data/model/product.dart';

class Scan extends StatefulWidget {
  @override
  ScanState createState() => ScanState();
}

class ScanState extends State<Scan> {
  ProductBloc productBloc;
  String value, QR = "";
  List<Product> products;

  @override
  void initState() {
    super.initState();
    productBloc = BlocProvider.of<ProductBloc>(context);
    productBloc.add(InitStateProduct());
    products = List<Product>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(title: "قراءة المنتجات ", color: Colors.white),
      body: BlocBuilder<ProductBloc, ProductState>(
        builder: (context, state) {
          if (state is ProductInitial) {
            return list1();
          }
          if (state is ProductErrorState) {
            return buildInitialInput();
          }
          if (state is ProductLoaded) {
            return list(state.product);
          }
        },
      ),
    );
  }
  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildInitialInput() {
    return Center(
      child: Text('Error Network'),
    );
  }
  Widget list1() {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .5,
              child: RaisedButton(
                color: Colors.redAccent,
                textColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      right: 4, left: 4, top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'قراءة  barcode',
                        style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                      ),
                      Icon(FontAwesomeIcons.barcode)
                    ],
                  ),
                ),
                onPressed: () {
                  QR_Scanner();
                },
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
              ),
            ),
          ),
        ),
        button(),
      ],
    );
  }
  Widget Barcode(){
    return Expanded(
      flex: 1,
      child: Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width * .5,
          child: RaisedButton(
            color: Colors.redAccent,
            textColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 4, left: 4, top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'قراءة  barcode',
                    style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                  ),
                  Icon(FontAwesomeIcons.barcode)
                ],
              ),
            ),
            onPressed: () {
              QR_Scanner();
            },
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget list(Product product) {

    products.add(product);

    return Column(
      children: [
        Barcode(),
        Expanded(
          flex: 10,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: products.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Row(children: [
                  Expanded(
                    flex: 2,
                    child: Text(products[index].name.toString()),
                  ),
                  Expanded(
                      flex: 2,
                      child: TextField(
                        // controller: _addProductToCart(counter),
                        decoration: InputDecoration(
                          labelText: "Qty",
                        ),
                      )),
                ]));
            },
          ),
        ),
        button()
      ],
    );
  }

  Widget button() {
    return Expanded(
        flex: 2,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              color: Colors.teal,
              textColor: Colors.white,
              child: Padding(
                padding:
                    const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'اتمام العملية',
                      style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                    ),
                    Icon(Icons.visibility)
                  ],
                ),
              ),
              onPressed: () {
                //Navigator.push(context, MaterialPageRoute(builder: (context)=> BeforeCategoryScreen()));
              },
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0),
              ),
            ),
            RaisedButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Padding(
                padding:
                    const EdgeInsets.only(right: 4, left: 4, top: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'الغاء',
                      style: TextStyle(fontFamily: "Cairo", fontSize: 13),
                    ),
                    Icon(Icons.visibility)
                  ],
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0),
              ),
            ),
          ],
        ));
  }

  QR_Scanner() async {
    QR = await FlutterBarcodeScanner.scanBarcode(
        "#004297", "Cancel", true, ScanMode.QR);

    setState(() {
      value = QR;
      productBloc.add(GetProduct(value));
    });
  }


}