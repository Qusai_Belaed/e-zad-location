import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/bloc/category/category_bloc.dart';
import 'package:location/data/repository/categorey_Rep.dart';
import 'package:location/pages/category/tap.dart';


class BeforeCategoryScreen extends StatefulWidget {
  @override
  _BeforeCategoryScreenState createState() => _BeforeCategoryScreenState();
}

class _BeforeCategoryScreenState extends State<BeforeCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => CategoryBloc(ApiCategoryRepository()),
      child: CategoryTap(),
    );
  }
}
