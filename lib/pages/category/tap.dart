import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/bloc/category/category_bloc.dart';
import 'package:location/data/model/category.dart';
import 'package:location/pages/widget/brand_widget.dart';
import 'package:location/pages/widget/category_widget.dart';
import 'package:location/pages/widget/shared.dart';

class CategoryTap extends StatefulWidget {
  @override
  CategoryTapState createState() => CategoryTapState();
}

class CategoryTapState extends State<CategoryTap> {
  CategoryBloc categoryBloc;

  @override
  void initState() {
    super.initState();
    categoryBloc = BlocProvider.of<CategoryBloc>(context);
    categoryBloc.add(GetAllCategory());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(title: "زيارة موقع", color: Colors.white),
      body: BlocBuilder<CategoryBloc, CategoryState>(
        builder: (context, state) {
          if (state is CategoryLoading) {
            return buildLoading();
          }
          if (state is CategoryErrorState) {
            return buildInitialInput();
          }
          if (state is CategoryLoaded) {
            return buildColumnWithData(state.category);
          }
        },
      ),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildColumnWithData(List<Category> category) {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: category.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return CategoryWidget(category[index]);
            },
          ),
        ),
        Expanded(
          flex: 10,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: category.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return BrandWidget(category[index]);
            },
          ),
        ),
      ],
    );
  }

  Widget buildInitialInput() {
    return Center(
      child: Text('Error Network'),
    );
  }
}
